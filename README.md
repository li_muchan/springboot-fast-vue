## springboot-fast-vue
springboot-fast-vue是springboot-fast项目的前端页面
使用vue.js ui使用element-ui

## 使用
```
# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npmmirror.com
# 启动服务
npm run dev
```
